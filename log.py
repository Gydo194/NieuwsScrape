#log functies
def log(string):
	print("[-] " + str(string))

def success_log(string):
	print("\033[92m[+] " + str(string) + "\033[0m")

def error_log(string):
	print("\033[91m[X] " + str(string) + "\033[0m")
