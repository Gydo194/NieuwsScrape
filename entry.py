from app import db
from query import Query

class Entry(db.Model):
    __tablename__ = 'entry'
    id = db.Column('id', db.Integer(), primary_key=True)

    data = db.Column('data', db.Text(0xFFFF)) #64 KiB
    title = db.Column('title', db.Text(0x2000)) #8 KiB
    uri = db.Column('uri', db.Text(0x2000)) # 8 KiB
    origin = db.Column('origin', db.String(100))
    timestamp = db.Column('timestamp', db.DateTime())
    content = db.Column('content', db.String(250))
    comment_count = db.Column('comment_count', db.Integer())
    retweet_count = db.Column('retweet_count', db.Integer())
    hyperlink = db.Column('hyperlink', db.String(300))
    pictureUrl = db.Column('pictureUrl', db.String(300))
    published_date = db.Column('published_date', db.String(32))
    edited_date = db.Column('edited_date', db.String(32))

    query_id = db.Column(db.Integer, db.ForeignKey('query.id'))
#    entry = db.relationship("Query", back_populates="query")


    def __repr__(self):
        return "<{klass} @{id:x} {attrs}>".format(
            klass=self.__class__.__name__,
            id=id(self) & 0xFFFFFF,
            attrs=" ".join("{}={!r}".format(k, v) for k, v in self.__dict__.items()),
            )
