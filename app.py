from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
 
#SQL_USER = "nieuws"
#SQL_PASS = "scraper"
#SQL_SERVER = "185.66.251.171"
#SQL_DB = "nieuwsscraper"

SQL_USER = "nieuws"
SQL_PASS = "scraper"
SQL_SERVER = "127.0.0.1"
SQL_DB = "nieuwsscraper"

app = Flask(__name__)
app.config['SECRET_KEY'] = 'ISTRUE!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + SQL_USER + ":" + SQL_PASS + "@" + SQL_SERVER + "/" + SQL_DB

bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
