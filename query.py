from app import db

class Query(db.Model):
    __tablename__ = 'query'
    id = db.Column('id', db.Integer(), primary_key=True)
    subject = db.Column('subject', db.String(12))
    results = db.Column('results', db.Integer())
    total_avg = db.Column('total_avg', db.Integer())
    start_scrape_time = db.Column('start_time', db.DateTime())
    end_scrape_time = db.Column('end_time', db.DateTime())
    first_article_time = db.Column('first_artc_time', db.DateTime())
    last_article_time = db.Column('last_artc_time', db.DateTime())
    entries = db.relationship("Entry", backref="query", lazy = True)
