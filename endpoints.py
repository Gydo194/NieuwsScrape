from app import app, db, bootstrap, login_manager

from entry import Entry
from query import Query

from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm 
from wtforms import StringField, TextField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from flask_sqlalchemy  import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask import Flask,abort,render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import DeclarativeMeta
from log import success_log, error_log, log
from io import StringIO
from datetime import date, datetime
from searchclient import search

import pymysql
import csv
import json



def new_alchemy_encoder(revisit_self = False, fields_to_expand = []):
    _visited_objs = []

    class AlchemyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, (datetime, date)):
                success_log("DATETIME ENCODER")
                return obj.strftime("%d/%m/%Y %H:%M")
            if isinstance(obj.__class__, DeclarativeMeta):
                # don't re-visit self
                if revisit_self:
                    if obj in _visited_objs:
                        return None
                    _visited_objs.append(obj)

                # go through each field in this SQLalchemy class
                fields = {}
                for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata' and not x.startswith('query')]:
                    val = obj.__getattribute__(field)

                    # is this field another SQLalchemy object, or a list of SQLalchemy objects?
                    if isinstance(val.__class__, DeclarativeMeta) or (isinstance(val, list) and len(val) > 0 and isinstance(val[0].__class__, DeclarativeMeta)):
                        # unless we're expanding this field, stop here
                        if field not in fields_to_expand:
                            # not expanding this field: set it to None and continue
                            fields[field] = None
                            continue

                    fields[field] = val
                # a json-encodable dict
                return fields

            return json.JSONEncoder.default(self, obj)

    return AlchemyEncoder

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class LoginForm(FlaskForm):
    username = StringField('Gebruikersnaam:', validators=[InputRequired(), Length(min=8, max=20)])
    password = PasswordField('Wachtwoord:', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('Onthoud mij')

class RegisterForm(FlaskForm):
    email = StringField('E-mailadres:', validators=[InputRequired(), Email(message='U heeft een ongeldig email adres ingevoerd.'), Length(max=50)])
    username = StringField('Gebruikersnaam:', validators=[InputRequired('Voer een geldig gebruikersnaam in.'), Length(min=8)])
    password = PasswordField('Wachtwoord:', validators=[InputRequired(), Length(min=8, max=80)])

@app.before_first_request
def init_db():
    success_log("Creating all tables...")
    db.create_all()

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                return render_template('home.html', form=form)
            else:
                return render_template('loginfailed.html', form=form)
        return render_template('loginfailed.html', form=form)
    return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()
    if form.validate_on_submit():
        try:
         hashed_password = generate_password_hash(form.password.data, method='sha256')
         new_user = User(username=form.username.data, email=form.email.data, password=hashed_password)
         db.session.add(new_user)
         db.session.commit()
         return redirect(url_for('dashboard'))
        except:
         return render_template('registerfailed.html', form=form)
    return render_template('register.html', form=form)


@app.route('/home')
@login_required
def dashboard():
    return render_template('home.html', name=current_user.username)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


   
#import CSV from a file or StringIO object.
#automatically calculates timestamp.
#needs query_subject.
def import_csv(file, query_subject):
    current_time = datetime.now()
    next(f) #skip column headers
    query = Query(subject = query_subject, timestamp = current_time)
    reader = csv.DictReader(file, fieldnames=("origin","timestamp","content","title","comment_count","retweet_count"), delimiter=",")
    for row in reader:
        #print("ROW %s" % row)
        r_origin = row["origin"]
        epoch_str = row["timestamp"].split(".0")[0]
        epoch_int = int(epoch_str)
        r_timestamp = datetime.utcfromtimestamp(epoch_int)
        r_content = row["content"]
        r_title = row["title"]
        r_comment_count = row["comment_count"]
        r_retweet_count = row["retweet_count"]
        
        print("ORIGIN = %s" % r_origin)
        print("TIMESTAMP = %s" % r_timestamp)
        print("CONTENT = %s" % r_content)
        print("TITLE = %s" % r_title)
        print("COMMENT_COUNT = %s" % r_comment_count)
        print("RETWEET_COUNT = %s" % r_retweet_count)

        csvrow = Entry(origin = r_origin, timestamp = r_timestamp, content = r_content, title = r_title, comment_count = r_comment_count, retweet_count = r_retweet_count)
        query.entries.append(csvrow)
        print("generated csvrow '%s'" % csvrow)
    db.session.add(query)

def generate_json(query_id):
    query = Query.query.get(query_id)
    assert 0 < len(query.entries)
    json_str = json.dumps(query, cls=new_alchemy_encoder(False, ['entries','subject', 'timestamp', 'id']), check_circular=True)
    success_log("generated json '%s'" % json_str)
    return json_str

@app.route("/get_query/<int:query_id>")
def get_json_query(query_id):
    success_log("serving JSON for query '%d' to client" % query_id)
    try:
        json_str = generate_json(query_id)
    except Exception as e:
        error_log("failed to generate json for query '%d': '%s'" % (query_id, e))
        abort(500)

    return json_str

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/meningen")
def mening():
    return render_template('meningen.html')

@app.route("/charts")
def grafiek():
    return render_template('charts.html')

@app.route("/search/<string:query>")
def importresults(query):
    assert isinstance(query, str), "importresults(): query is not a string, got '%s' instead" % str(type(query))
    success_log("importresults(): importing results for query '%s'" % query)
    search(query)
    return "OK"
