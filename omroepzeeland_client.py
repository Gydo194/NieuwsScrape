import requests
import json

from log import error_log, success_log, log
from pprint import pprint as pp
from functools import reduce
from entry import Entry

import datetime

OZ_SEARCH_URI = "https://www.omroepzeeland.nl/zoeken/Results?query={}&from=&to=&page={}&searchItemType="

def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)

def oz_search_request(query, pagecount):
    url = ""
    data = ""
    assert isinstance(query, str), "oz_search_request: query is not a string, got '%s' instead" % str(type(query))
    assert isinstance(pagecount, int), "oz_search_request: pagecount is not an int, got '%s' instead" % str(type(pagecount))
    try:
        url = OZ_SEARCH_URI.format(query, pagecount)
    except Exception as e:
        error_log("failed to format URL: '%s'" % e)
    try:
        response = requests.get(url)
        data = response.json()
    except Exception as e:
        error_log("failed to request JSON: '%s'" % e)
        data = {}
    return data


def parse_timestamp(timestring):
    assert isinstance(timestring, str), "oz_search: query is not a string, got '%s' instead" % str(type(timestring))
#    try:
#        date = parser.parse(timestring, fuzzy=True)
#    except Exception as e:
#        error_log("Failed to parse date from string '%s': '%s'" % (timestring, e))
#        return datetime.datetime(1970,1,1,0,0)
    return datetime.datetime(1970,1,1,0,0)


def parse_oz_json(json):
    entries = []
    assert isinstance(json, dict), "parse_oz_json: json is not a dict, got '%s' instead" % str(type(json))

    results = deep_get(json, "searchResults", dict())
    pagecount = int(deep_get(json, "pageCount", 1))

    for result in results:
        uri = deep_get(result, "url", "No URI")
        title = deep_get(result, "title", "No title")
        content = deep_get(result, "description", "No description")
        timestamp = parse_timestamp(deep_get(result, "publicationTimeString", "No timestamp"))
        entry = Entry()
        entry.uri = uri
        entry.title = title
        entry.data = content
        entry.timestamp = timestamp
        entry.origin = "Omroep Zeeland"
        entries.append(entry)
    return entries, pagecount

def oz_search(query):
    assert isinstance(query, str), "oz_search: query is not a string, got '%s' instead" % str(type(json))
    pagecount = 1
    results = []

    json = oz_search_request(query, pagecount)
    initial_results, pagecount = parse_oz_json(json)
    success_log(pagecount)
    #success_log(json)
    results = initial_results

    for count in range(1, pagecount):
        try:
            response = oz_search_request(query, count)
            additional_results, prop = parse_oz_json(response)
            results =  results + additional_results
            #success_log(response)
        except Exception as e:
            error_log("Failed to process json: '%s'" % e)
    return results



#articles = oz_search("windmolen")
#success_log(str(type(articles)))
#pp(articles)
