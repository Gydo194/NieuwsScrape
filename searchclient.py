from entry import Entry
from query import Query

from log import error_log, success_log, log

from omroepzeeland_client import oz_search

from app import db

def search(query):
    assert isinstance(query, str), "search(): query is not a string, got '%s' instead" % str(type(query))
    #import data from all sources
    oz_import(query)

def oz_import(query):
    assert isinstance(query, str), "searchlient.oz_import: query is not a string. got '%s' instead" % str(type(query))
    query_obj = Query()
    query_obj.subject = query

    entries = oz_search(query)
    for i in entries:
        query_obj.entries.append(i)
        db.session.add(i)
    try:
        db.session.add(query_obj)
        db.session.commit()
    except Exception as e:
        error_log("Failed to commit session: '%s'" % e)
